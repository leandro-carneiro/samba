FROM alpine
MAINTAINER lcarneiro@simplesmenteuse.com

ADD entrypoint /

RUN apk update && \
apk add samba samba-common-tools && \
echo -en "[global]\n        workgroup = SAMBA\n        min protocol = SMB2\n        security = user\n        map to guest = Bad User\n        passdb backend = tdbsam\n        guest account = root\n        printing = cups\n        printcap name = cups\n        load printers = yes\n        cups options = raw\n\n" > /etc/samba/smb.conf && \
chmod +x /entrypoint

ENTRYPOINT /entrypoint

EXPOSE 137/udp
EXPOSE 138/udp
EXPOSE 139/tcp
EXPOSE 445/tcp
